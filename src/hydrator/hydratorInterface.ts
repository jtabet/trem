export interface PropertyHydrator {
    hydrate(target: any, rawData: any): void;
}

export interface EntityHydrator {
    hydrate<T>(target: new () => T, data: any): Iterable<T>
}
