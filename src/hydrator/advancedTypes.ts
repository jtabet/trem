import {PropertyHydrator} from './hydratorInterface';
import {EntityArrayHydrator} from './entityHydrators';
import {getValueRecursive} from './helpers';

export class EntityCollection implements PropertyHydrator {
    protected target: string;
    protected source: string;
    protected entityClass: new () => any;

    public constructor(target: string, config: any) {
        this.target = target;
        this.source = config.source;
        this.entityClass = config.entityClass;
    }

    public hydrate(target: any, rawData: any): void {
        const value: any[] = getValueRecursive(rawData, this.source);
        const hydrator = new EntityArrayHydrator();

        target[this.target] = [...hydrator.hydrate(this.entityClass, value)];
    }
}
