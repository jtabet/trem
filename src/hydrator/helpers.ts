export function getValueRecursive(data: any, key: string): any {
  if (typeof data !== "object" || data[key] === null) {
    return undefined;
  }

  const i = key.indexOf('.');

  if (i === -1) {
    return data[key];
  }

  const currentKey = key.slice(0, Math.max(i, 0));
  const remainingKey = key.slice(i + 1);

  return getValueRecursive(data[currentKey], remainingKey);
}
