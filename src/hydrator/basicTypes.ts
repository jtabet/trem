import {PropertyHydrator} from './hydratorInterface';
import {getValueRecursive} from './helpers';

export class Boolean implements PropertyHydrator {
    protected target: string;
    protected source: string;

    public constructor(target: string, config: any) {
        this.target = target;
        this.source = config.source;
    }

    public hydrate(target: any, rawData: any): void {
        target[this.target] = !!getValueRecursive(rawData, this.source);
    }
}

export class String implements PropertyHydrator {
    protected target: string;
    protected source: string;
    protected optional: boolean;

    public constructor(target: string, config: any) {
        this.target = target;
        this.source = config.source;
        this.optional = config.optional == true;
    }

    public hydrate(target: any, rawData: any): void {
        const value = getValueRecursive(rawData, this.source);

        if (typeof value === 'string') {
            target[this.target] = value;
            return;
        }

        if (value == undefined && this.optional) {
            target[this.target] = "";
            return;
        }

        throw new Error(
            "Hydrator.string:\n"+
            "  Invalid type provided in value for "+this.target+"\n"+
            "  Should be a string, got "+JSON.stringify(value)
        );
    }
}

export class Number implements PropertyHydrator {
    protected target: number;
    protected source: string;
    protected optional: boolean;
    
    public constructor(target: number, config: any) {
        this.target = target;
        this.source = config.source;
        this.optional = config.optional == true;
    }

    public hydrate(target: any, rawData: any): void {
        const value = getValueRecursive(rawData, this.source);

        if (typeof value === 'number') {
            target[this.target] = value;
            return;
        }
        
        if (value == undefined && this.optional) {
            target[this.target] = 0;
            return;
        }

        throw new Error(
            "Hydrator.number:\n"+
            "  Invalid type provided in value for "+this.target+"\n"+
            "  Should be a number, got "+JSON.stringify(value)
        );
    }
}

export class Datetime implements PropertyHydrator {
    protected target: string;
    protected source: string;

    public constructor(target: string, config: any) {
        this.target = target;
        this.source = config.source;
    }

    public hydrate(target: any, rawData: any): void {
        target[this.target] = new Date(getValueRecursive(rawData, this.source));
    }
}

export class Raw implements PropertyHydrator {
    protected target: string;
    protected source: string;

    public constructor(target: string, config: any) {
        this.target = target;
        this.source = config.source;
    }

    public hydrate(target: any, rawData: any): void {
        target[this.target] = getValueRecursive(rawData, this.source);
    }
}
