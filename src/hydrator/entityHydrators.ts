import {EntityHydrator} from "./hydratorInterface";

export class BasicEntityHydrator implements EntityHydrator {
    public * hydrate<T>(target: new () => T, data: any): Iterable<T> {
        const entityInstance = new target();
        let metadata = target.prototype._meta.properties;

        for (let property in metadata) {
            const propertyHydrator = new metadata[property]['hydrator'](property, metadata[property]);
            propertyHydrator.hydrate(entityInstance, data);
        }

        yield entityInstance;
    }
}

export class EntityArrayHydrator implements EntityHydrator {
    public * hydrate<T>(target: new () => T, data: any): Iterable<T> {
        let metadata = target.prototype._meta.properties;

        for (const row of data) {
            const entityInstance = new target();

            for (let property in metadata) {
                const propertyHydrator = new metadata[property]['hydrator'](property, metadata[property]);
                propertyHydrator.hydrate(entityInstance, row);
            }

            yield entityInstance;
        }
    }
}
