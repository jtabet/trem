import * as BasicHydrators from '../hydrator/basicTypes';

function Property(hydratorConfig: {[key: string]: any}): (target: any, property: string) => void {
    return function (target: any, property: string): void {
        if (target['_meta'] === undefined) {
            target['_meta'] = {};
        }

        if (target['_meta']['properties'] === undefined) {
            target['_meta']['properties'] = {};
        }

        target['_meta']['properties'][property] = hydratorConfig;
    }
}

export function Boolean(source: string): (target: any, property: string) => void {
    return Property({'hydrator': BasicHydrators.Boolean, source});
}

export function String(source: string, optional: boolean): (target: any, property: string) => void {
    return Property({'hydrator': BasicHydrators.String, source, optional});
}

export function Number(source: string, optional: boolean): (target: any, property: string) => void {
    return Property({'hydrator': BasicHydrators.Number, source, optional});
}

export function Datetime(source: string): (target: any, property: string) => void {
    return Property({'hydrator': BasicHydrators.Datetime, source});
}

export function Raw(source: string): (target: any, property: string) => void {
    return Property({'hydrator': BasicHydrators.Raw, source});
}

