import {EntityHydrator} from "../hydrator/hydratorInterface";

export function Entity(endpoints: {[selector: string]: new() => EntityHydrator}): (target: new() => any) => void {
    return function (target: new() => any): void {
        if (target.prototype['_meta'] === undefined) {
            target.prototype['_meta'] = {};
        }

        target.prototype['_meta']['endpoints'] = endpoints;
    }
}

