import {EntityHydrator} from "./hydrator/hydratorInterface";

export class ResponseHandler {
    public constructor(protected managedEntities: (new () => any)[]) {}

    public handle<T>(url: string, responseData: any): T[] {
        let results: any[] = [];

        for (const entity of this.managedEntities) {
            for (const endpointSelector of Object.keys(entity.prototype['_meta']['endpoints'])) {
                const regexp = new RegExp(endpointSelector);

                if (regexp.test(url)) {
                    // TODO: Find a way to allow constructor parameters for custom hydrators
                    const hydrator: EntityHydrator = new entity.prototype['_meta']['endpoints'][endpointSelector]();

                    results = [...results, ...hydrator.hydrate(entity, responseData)];
                }
            }
        }

        return results;
    }
}
