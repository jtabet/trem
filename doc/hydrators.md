# Hydrators

## Default hydrators

### Entity hydrators

**BasicEntityHydrator**: This is the basic entity hydrator. It will map one object to one entity.

```typescript
// Sample data for the BasicEntityHydrator
{
    "id": 10,
    "variety": "Purple Viking",
    "color": "not purple"
}
```

**EntityArrayHydrator**: This hydrator handles arrays of objects or objects containing objects (if your keys are not numeric for instance).

```typescript
// Sample data for the EntityArrayHydrator
[
    {"id": 1, "isVegetable": true},
    {"id": 2, "isVegetable": true},
    {"id": 3, "isVegetable": true},
    {"id": 4, "isVegetable": true},
    {"id": 5, "isVegetable": true},
    {"id": 6, "isVegetable": true},
    {"id": 7, "isVegetable": true},
    {"id": 8, "isVegetable": true},
    {"id": 9, "isVegetable": true},
    {"id": 10, "isVegetable": true}
]
```

### Property hydrators

**Boolean**

**String**

**Number**

**Datetime**

**Raw**

**EntityCollection**

## Create your own hydrators

You can extend the library's functionalities by creating your own hydrators, which allow you to handle any api format. For this, you have to implement the `EntityHydrator` interface in your class, then use it in your `@Entity()` annotation.

```typescript
// Sample custom hydrator
export class HalCarrotCollectionHydrator implements EntityHydrator {
    public * hydrate<T>(target: new () => T, data: any): Iterable<T> {
        let metadata = target.prototype._meta.properties;

        for (const key of Object.keys(data['_embedded']['carrots'])) {
            const entityInstance = new target();
            
            for (let property in metadata) {
                const propertyHydrator = new metadata[property]['hydrator'](property, metadata[property]);
                propertyHydrator.hydrate(entityInstance, data['_embedded']['carrots'][key]);
            }

            yield entityInstance;
        }
    }
}
```