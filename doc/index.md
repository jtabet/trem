# Index

- [Decorators](decorators.md)
    - [Entities](decorators.md#entities)
    - [Properties](decorators.md#properties)
- [Hydrators](hydrators.md)
    - [Default hydrators](hydrators.md#default-hydrators)
    - [Create your own hydrators](hydrators.md#create-your-own-hydrators)
