# Decorators

## Entities

**Entity**: This annotation is used to detect which entity to return and when. It takes a single argument, an object with a regexp pattern as key to match the url(s) and the hydrator class to use as value.

```typescript
// Sample usage for the Entity annotation
@Entity({
    '(?<!hal\/)carrots$': EntityArrayHydrator,
    'hal\/carrots$': HalCarrotCollectionHydrator,
    'legacy_data$': MixedDataCarrotHydrator
})
export class Carrot  {

}
```

## Properties

**Boolean**: This annotation is used to map a boolean property. It takes a single argument, the key path to get the value.

**String**: This annotation is used to map a string property. It takes a two arguments, the key path to get the value and whether the value is optional or not.

**Number**: This annotation is used to map a number property. It takes a two arguments, the key path to get the value and whether the value is optional or not.

**Datetime**: This annotation is used to map a datetime property. It takes a single argument, the key path to get the value.

**Raw**: This annotation is used to map a raw property. It takes a single argument, the key path to get the value.
