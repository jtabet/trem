<div align="center">
  <a href="https://gitlab.com/jtabet/trem">
    <img src="https://gitlab.com/jtabet/trem/-/raw/master/doc/assets/trem-logo.png" alt="Logo" width="150" height="150">
  </a>

  <p>A lightweight typescript library to handle the mapping of entities to REST APIs</p>
</div>

## Installation

The library is available as a package on [npm](https://www.npmjs.com/package/trem).

```bash
npm install --save trem
```

## Usage

First, configure your entity/entities using the default annotations/hydrators, [or create your own](doc/hydrators.md#create-your-own-hydrators).

```typescript
import { Entity, Number, String, BasicEntityHydrator } from 'trem';

@Entity({'potato$': BasicEntityHydrator})
export class Potato {
    @Number('id', false) public id: number;
    @String('variety', true) public variety: string;
    @String('color', true) public color: string;
    @String('locations.0.country', true) public mainCountry: string;

    public isCommon(): boolean {
        return this.color === 'yellow';
    }

    // Add your own logic here
}
```

Then, use the responseHandler to do the mapping for you.

```typescript
import { ResponseHandler } from 'trem';
import { Potato } from './potato';

class MyApiCaller {
  protected entityMapper: ResponseHandler;

  constructor() {
    this.entityMapper = new ResponseHandler([Potato /* Add your other entities here */])
  }

  public getData<T>(): T[] {
    // Your api call logic here
    return this.entityMapper.handle(url, responseData);
  }
}
```
