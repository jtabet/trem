import {Carrot} from './entities/carrot';

describe("Check if metadata is emitted from decorators", function() {
    var c = new Carrot();
  
    it("must contain a _meta in prototype", function() {
      expect(c.constructor.prototype._meta).toBeTruthy();
    });

    it("must contain the entity metadata", function() {
        expect(c.constructor.prototype._meta.endpoints).toBeTruthy();
        expect(Object.keys(c.constructor.prototype._meta.endpoints).length).toEqual(3);
    });

    it("must contain the property metadata", function() {
        expect(c.constructor.prototype._meta.properties).toBeTruthy();
        expect(c.constructor.prototype._meta.properties['id']).toBeTruthy();
    });
});
