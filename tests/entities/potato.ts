import { Entity, Number, String, BasicEntityHydrator } from "../../src/public_api";
import { MixedDataPotatoHydrator } from "../customHydrators/mixed";

@Entity({
    'potato$': BasicEntityHydrator,
    'legacy_data$': MixedDataPotatoHydrator
})
export class Potato  {
    @Number('id', false) public id: number;
    @String('variety', true) public variety: string;
    @String('color', true) public color: string;
    @String('locations.0.country', true) public mainCountry: string;

    public isCommon(): boolean {
        return this.color === 'yellow';
    }

    public canKillYou(): boolean {
        return this.variety === 'Purple Viking' || this.color === 'purple';
    }
}
