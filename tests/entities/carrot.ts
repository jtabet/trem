import { Entity, Number, EntityArrayHydrator } from "../../src/public_api";
import { HalCarrotCollectionHydrator } from "../customHydrators/halCollection";
import { MixedDataCarrotHydrator } from "../customHydrators/mixed";

@Entity({
    '(?<!hal\/)carrots$': EntityArrayHydrator,
    'hal\/carrots$': HalCarrotCollectionHydrator,
    'legacy_data$': MixedDataCarrotHydrator
})
export class Carrot  {
    @Number('id', false) public id: number;
    public isVegetable = false;
}
