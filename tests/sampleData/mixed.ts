export const data = {
    "data": {
        "potatoes": {
            "a": {
                "id": 1,
                "variety": "Yukon Gold",
                "color": "yellow"
            },
            "b": {
                "id": 1,
                "variety": "Yukon Gold",
                "color": "yellow"
            },
            "c": {
                "id": 1,
                "variety": "Yukon Gold",
                "color": "yellow"
            },
            "d": {
                "id": 1,
                "variety": "Yukon Gold",
                "color": "yellow"
            },
            "e": {
                "id": 2,
                "variety": "Purple Peruvian",
                "color": "purple"
            },
            "f": {
                "id": 3,
                "variety": "Idaho Russet",
                "color": "yellow"
            },
            "g": {
                "id": 4,
                "variety": "Katahdin",
                "color": "yellow"
            },
            "h": {
                "id": 5,
                "variety": "Red Bliss",
                "color": "red"
            },
            "i": {
                "id": 6,
                "variety": "New Potatoes",
                "color": "unknown"
            },
            "j": {
                "id": 7,
                "variety": "Adirondack Blue",
                "color": "purple"
            },
            "k": {
                "id": 8,
                "variety": "Adirondack Red",
                "color": "red"
            },
            "l": {
                "id": 9,
                "variety": "Fingerling",
                "color": "yellow"
            },
            "m": {
                "id": 10,
                "variety": "Purple Viking",
                "color": "not purple"
            }
        },
        "carrots": [
            {"id":1, "isVegetable": true},
            {"id":2, "isVegetable": true},
            {"id":3, "isVegetable": true},
            {"id":4, "isVegetable": true},
            {"id":5, "isVegetable": true},
            {"id":6, "isVegetable": true},
            {"id":7, "isVegetable": true},
            {"id":8, "isVegetable": true},
            {"id":9, "isVegetable": true},
            {"id":10, "isVegetable": true}
        ]
    },
    "page": 1,
    "items": 10,
    "total": 1000
}