export const data = {
    "potato": {
        "id": 10,
        "variety": "Purple Viking",
        "color": "not purple",
        "locations": [
            {
                "country": "Chile",
                "lat": -35.4136367,
                "lon": -106.2002608
            },
            {
                "country": "Peru",
                "lat": -9.2047273,
                "lon": -79.514733
            }
        ]
    }
}
