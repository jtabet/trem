export const data = {
    "_links": {
        "self": {
        "href": "http://example.com/api/book/hal-cookbook"
        },
        "next": {
        "href": "http://example.com/api/book/hal-case-study"
        },
        "prev": {
        "href": "http://example.com/api/book/json-and-beyond"
        },
        "first": {
        "href": "http://example.com/api/book/catalog"
        },
        "last": {
        "href": "http://example.com/api/book/upcoming-books"
        }
    },
    "_embedded": {
        "carrots": [
            {"id":1, "isVegetable": true},
            {"id":2, "isVegetable": true},
            {"id":3, "isVegetable": true},
            {"id":4, "isVegetable": true},
            {"id":5, "isVegetable": true},
            {"id":6, "isVegetable": true},
            {"id":7, "isVegetable": true},
            {"id":8, "isVegetable": true},
            {"id":9, "isVegetable": true},
            {"id":10, "isVegetable": true}
        ]
    },
    "id": "hal-cookbook",
    "name": "HAL Cookbook"
}
