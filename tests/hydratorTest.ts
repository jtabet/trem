import {BasicEntityHydrator} from '../src/public_api';
import {Potato} from './entities/potato';
import {data} from './sampleData/singleEntity';
import {data as multilevel} from './sampleData/multilevel';

describe("Check if the hydrators hydrate properly", function() {
    const hydrator = new BasicEntityHydrator();

    it("must be able to hydrate without error", function() {
        hydrator.hydrate(Potato, data);
    });

    it("must be able to hydrate properly", function() {
        const p = [...hydrator.hydrate(Potato, data)][0];

        expect(p.id).toBe(data.id);
        expect(p.variety).toBe(data.variety);
        expect(p.color).toBe(data.color);
        expect(p.canKillYou()).toBeTrue();
    });

    it("must be able to hydrate multi-level data", function() {
        const p = [...hydrator.hydrate(Potato, multilevel['potato'])][0];

        expect(p.mainCountry).toBe('Chile');
    });
});
