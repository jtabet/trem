import { EntityHydrator } from "../../src/public_api";

export class HalCarrotCollectionHydrator implements EntityHydrator {
    public * hydrate<T>(target: new () => T, data: any): Iterable<T> {
        let metadata = target.prototype._meta.properties;

        for (const key of Object.keys(data['_embedded']['carrots'])) {
            const entityInstance = new target();
            
            for (let property in metadata) {
                const propertyHydrator = new metadata[property]['hydrator'](property, metadata[property]);
                propertyHydrator.hydrate(entityInstance, data['_embedded']['carrots'][key]);
            }

            yield entityInstance;
        }
    }
}
