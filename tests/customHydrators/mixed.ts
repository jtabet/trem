import { EntityHydrator } from "../../src/public_api";

export class MixedDataPotatoHydrator implements EntityHydrator {
    public * hydrate<T>(target: new () => T, data: any): Iterable<T> {
        let metadata = target.prototype._meta.properties;

        for (const key of Object.keys(data['data']['potatoes'])) {
            const entityInstance = new target();
            
            for (let property in metadata) {
                const propertyHydrator = new metadata[property]['hydrator'](property, metadata[property]);
                propertyHydrator.hydrate(entityInstance, data['data']['potatoes'][key]);
            }

            yield entityInstance;
        }
    }
}

export class MixedDataCarrotHydrator implements EntityHydrator {
    public * hydrate<T>(target: new () => T, data: any): Iterable<T> {
        let metadata = target.prototype._meta.properties;

        for (const key of Object.keys(data['data']['carrots'])) {
            const entityInstance = new target();
            
            for (let property in metadata) {
                const propertyHydrator = new metadata[property]['hydrator'](property, metadata[property]);
                propertyHydrator.hydrate(entityInstance, data['data']['carrots'][key]);
            }

            yield entityInstance;
        }
    }
}
