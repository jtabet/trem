import {ResponseHandler} from '../src/responseHandler';
import {Carrot} from './entities/carrot';
import {Potato} from './entities/potato';
import {data as entityListData} from './sampleData/list';
import {data as halEntityData} from './sampleData/hal';
import {data as singleEntityData} from './sampleData/singleEntity';
import {data as mixedData} from './sampleData/mixed';

describe("Check if the response handler handles api responses", function() {  
    const responseHandler = new ResponseHandler([Potato, Carrot]);

    it("handles entity lists", function() {
        const url = 'https://example.com/carrots';
        const result = responseHandler.handle(url, entityListData);
        expect(result).toBeTruthy();
        expect(result.length).toBe(10);
        expect(result[0].constructor).toBe(Carrot);
    });
    
    it("handles entities in hal format", function() {
        const url = 'https://example.com/hal/carrots';
        const result = responseHandler.handle(url, halEntityData);
        expect(result).toBeTruthy();
        expect(result.length).toBe(10);
        expect(result[0].constructor).toBe(Carrot);
    });
    
    it("handles single entity", function() {
        const url = 'https://example.com/potato';
        const result = responseHandler.handle(url, singleEntityData);
        expect(result).toBeTruthy();
        expect(result.length).toBe(1);
        expect(result[0].constructor).toBe(Potato);
    });

    it("handles entities with mixed data", function() {
        const url = 'https://example.com/legacy_data';
        const result = responseHandler.handle(url, mixedData);
        expect(result).toBeTruthy();
        expect(result.length).toBe(23);
        expect(result[0].constructor).toBe(Potato);
        expect(result[22].constructor).toBe(Carrot);
    });
});
